# project/api/users.py

from flask import Blueprint, request
from flask_restplus import Api, Resource, fields

from project import db
from project.api.services import (
    add_user,
    delete_user,
    get_all_users,
    get_user_by_email,
    get_user_by_id,
    update_user,
)

users_blueprint = Blueprint("users", __name__)
api = Api(users_blueprint)


user = api.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class UsersList(Resource):
    @api.marshal_with(user, as_list=True)
    def get(self):
        return get_all_users(), 200

    def post(self):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_email(email)  # User.query.filter_by(email=email).first()
        if user:
            response_object["message"] = "Sorry. That email already exists"
            return response_object, 400
        try:
            add_user(username, email)
            response_object["message"] = f"{email} was added!"
            return response_object, 201
        except Exception:
            db.session.rollback()
            response_object["message"] = f"Input payload validation failed"
            return response_object, 400


class Users(Resource):
    @api.marshal_with(user)
    def get(self, user_id):
        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            api.abort(404, f"user {user_id} does not exist")
        return user, 200

    def delete(self, user_id):
        response_object = {}
        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            api.abort(404, f"User {user_id} does not exist")
        # db.session.delete(user)
        # db.session.commit()
        delete_user(user)
        response_object["message"] = f"{user.email} was removed!"
        return response_object, 200

    @api.expect(user, validate=True)
    def put(self, user_id):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            api.abort(404, f"User {user_id} does not exist")
        # user.username = username
        # user.email = email
        # db.session.commit()
        update_user(user, username, email)
        response_object["message"] = f"{user.id} was updated!"
        return response_object, 200


api.add_resource(Users, "/users/<int:user_id>")
api.add_resource(UsersList, "/users")
