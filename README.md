# TDD lab
 ![Made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-success)

 [![pipeline status](https://gitlab.com/oihana.leiza/practica_ci_tdd/badges/master/pipeline.svg)](https://gitlab.com/oihana.leiza/practica_ci_tdd/commits/master)


En este proyecto se despliega una aplicación dockerizada con el fin de ejecutar tests y aplicar la metodología TDD.

## Evidencias

El conjunto de evidencias que demuestran la realización del proyecto se en cuantran en el siguiente documento: [evidencias](Evidencias.pdf)



## Autora

Oihana Leiza - oihana.leiza@alumni.mondragon.edu


##  License
Este proyecto tiene la licencia [Licencia MIT] (https://choosealicense.com/licenses/mit/), consulte el archivo LICENSE.md para obtener más detalles.
Las plantillas y las diferencias entre los tipos de licencia se pueden obtener en sitios web como  choosealicense or creativecommons.
